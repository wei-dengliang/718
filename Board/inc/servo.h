#ifndef _SERVO_H_
#define _SERVO_H_

#define	S_P_RCC_APBPeriph_TIMx_GPIOx1		RCC_APB2Periph_GPIOB
#define S_P_RCC_APBxPeriphClockCmd1         RCC_APB2PeriphClockCmd
#define	S_RCC_APBPeriph_TIMx1				RCC_APB2Periph_TIM10
#define S_RCC_APBxPeriphClockCmd1           RCC_APB2PeriphClockCmd
#define	SERVO1_PORT							GPIOB
#define SERVO1_TIM							TIM10
#define SERVO1_TIM_CH1_PIN					GPIO_Pin_8


#define	S_P_RCC_APBPeriph_TIMx_GPIOx2		RCC_APB2Periph_GPIOB
#define S_P_RCC_APBxPeriphClockCmd2         RCC_APB2PeriphClockCmd
#define	S_RCC_APBPeriph_TIMx2				RCC_APB2Periph_TIM11
#define S_RCC_APBxPeriphClockCmd2           RCC_APB2PeriphClockCmd
#define	SERVO2_PORT							GPIOB
#define SERVO2_TIM							TIM11
#define SERVO2_TIM_CH1_PIN					GPIO_Pin_9


#define	S_P_RCC_APBPeriph_TIMx_GPIOx3		RCC_APB2Periph_GPIOF
#define S_P_RCC_APBxPeriphClockCmd3         RCC_APB2PeriphClockCmd
#define	S_RCC_APBPeriph_TIMx3				RCC_APB1Periph_TIM13
#define S_RCC_APBxPeriphClockCmd3           RCC_APB1PeriphClockCmd
#define	SERVO3_PORT							GPIOF
#define SERVO3_TIM							TIM13
#define SERVO3_TIM_CH1_PIN					GPIO_Pin_8


#define	S_P_RCC_APBPeriph_TIMx_GPIOx4		RCC_APB2Periph_GPIOF
#define S_P_RCC_APBxPeriphClockCmd4         RCC_APB2PeriphClockCmd
#define	S_RCC_APBPeriph_TIMx4				RCC_APB1Periph_TIM14
#define S_RCC_APBxPeriphClockCmd4           RCC_APB1PeriphClockCmd
#define	SERVO4_PORT							GPIOF
#define SERVO4_TIM							TIM14
#define SERVO4_TIM_CH1_PIN					GPIO_Pin_9

#define servo_1 1
#define servo_2 2
#define servo_3 3
#define servo_4 4

extern void TIM_SERVO_Init(void);
extern void Servo_Open(void);
extern void Servo_Close(void);
extern void Test_Servo(void);
extern void Servo_Run(int servo_num, int speed_set);
#endif
