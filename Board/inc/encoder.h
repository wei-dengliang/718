#ifndef __ENCODER__
#define __ENCODER__

#include "stm32f10x.h"

#define	E_P_RCC_APBPeriph_TIMx_GPIOx1		RCC_APB2Periph_GPIOB
#define E_P_RCC_APBxPeriphClockCmd1         RCC_APB2PeriphClockCmd
#define	E_RCC_APBPeriph_TIMx1				RCC_APB1Periph_TIM3
#define E_RCC_APBxPeriphClockCmd1           RCC_APB1PeriphClockCmd
#define	ENCODER1_PORT						GPIOB
#define ENCODER1_TIM						TIM3
#define ENCODER1_TIM_CH1_PIN				GPIO_Pin_4
#define ENCODER1_TIM_CH2_PIN				GPIO_Pin_5

#define	E_P_RCC_APBPeriph_TIMx_GPIOx2		RCC_APB2Periph_GPIOB
#define E_P_RCC_APBxPeriphClockCmd2         RCC_APB2PeriphClockCmd
#define	E_RCC_APBPeriph_TIMx2				RCC_APB1Periph_TIM4
#define E_RCC_APBxPeriphClockCmd2           RCC_APB1PeriphClockCmd
#define	ENCODER2_PORT						GPIOB
#define ENCODER2_TIM						TIM4
#define ENCODER2_TIM_CH1_PIN				GPIO_Pin_6
#define ENCODER2_TIM_CH2_PIN				GPIO_Pin_7

#define	E_P_RCC_APBPeriph_TIMx_GPIOx3		RCC_APB2Periph_GPIOA
#define E_P_RCC_APBxPeriphClockCmd3         RCC_APB2PeriphClockCmd
#define	E_RCC_APBPeriph_TIMx3				RCC_APB1Periph_TIM2
#define E_RCC_APBxPeriphClockCmd3           RCC_APB1PeriphClockCmd
#define	ENCODER3_PORT						GPIOA
#define ENCODER3_TIM						TIM2
#define ENCODER3_TIM_CH1_PIN				GPIO_Pin_15
#define ENCODER3_TIM_CH2_PIN				GPIO_Pin_5//GPIOB的

#define	E_P_RCC_APBPeriph_TIMx_GPIOx4		RCC_APB2Periph_GPIOC
#define E_P_RCC_APBxPeriphClockCmd4         RCC_APB2PeriphClockCmd
#define	E_RCC_APBPeriph_TIMx4				RCC_APB2Periph_TIM8
#define E_RCC_APBxPeriphClockCmd4           RCC_APB2PeriphClockCmd
#define	ENCODER4_PORT						GPIOC
#define ENCODER4_TIM						TIM8
#define ENCODER4_TIM_CH1_PIN				GPIO_Pin_6
#define ENCODER4_TIM_CH2_PIN				GPIO_Pin_7

void Encoder_Init(void);
int read_encoder (TIM_TypeDef* ENCODER_TIM);

#endif
