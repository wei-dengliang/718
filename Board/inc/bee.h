#ifndef __BEE__
#define __BEE__

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"

#define BEE_GPIO_PORT GPIOA               /* GPIO端口 */
#define BEE_GPIO_CLK RCC_APB2Periph_GPIOA /* GPIO端口时钟 */
#define BEE_GPIO_PIN GPIO_Pin_12          /* 连接到SCL时钟线的GPIO */

/* 直接操作寄存器的方法控制IO */
#define digitalHi(p, i) \
    {                   \
        p->BSRR = i;    \
    } //输出为高电平
#define digitalLo(p, i) \
    {                   \
        p->BRR = i;     \
    } //输出低电平
#define digitalToggle(p, i) \
    {                       \
        p->ODR ^= i;        \
    } //输出反转状态

/* 定义控制IO的宏 */
#define BEE_TOGGLE digitalToggle(BEE_GPIO_PORT, BEE_GPIO_PIN)
#define BEE_OFF digitalHi(BEE_GPIO_PORT, BEE_GPIO_PIN)
#define BEE_ON digitalLo(BEE_GPIO_PORT, BEE_GPIO_PIN)

void BEE_GPIO_Init(void);
void BEE_Set(uint8_t State);
		
#endif
