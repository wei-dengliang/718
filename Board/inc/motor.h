#ifndef _MORTOR_H_
#define _MORTOR_H_

#define	M_P_RCC_APBPeriph_TIMx_GPIOx1		RCC_APB2Periph_GPIOA
#define M_P_RCC_APBxPeriphClockCmd1         RCC_APB2PeriphClockCmd
#define	M_RCC_APBPeriph_TIMx1				RCC_APB2Periph_TIM1
#define M_RCC_APBxPeriphClockCmd1           RCC_APB2PeriphClockCmd
#define	MOTOR1_PORT							GPIOA
#define MOTOR1_TIM							TIM1
#define MOTOR1_TIM_CH1_PIN					GPIO_Pin_8
#define MOTOR1_TIM_CH2_PIN					GPIO_Pin_9
#define MOTOR1_TIM_CH3_PIN                  GPIO_Pin_10
#define MOTOR1_TIM_CH4_PIN                  GPIO_Pin_11

#define	M_P_RCC_APBPeriph_TIMx_GPIOx2		RCC_APB2Periph_GPIOB
#define M_P_RCC_APBxPeriphClockCmd2         RCC_APB2PeriphClockCmd
#define	M_RCC_APBPeriph_TIMx2				RCC_APB1Periph_TIM12
#define M_RCC_APBxPeriphClockCmd2           RCC_APB1PeriphClockCmd
#define	MOTOR2_PORT							GPIOB
#define MOTOR2_TIM							TIM12
#define MOTOR2_TIM_CH1_PIN					GPIO_Pin_14
#define MOTOR2_TIM_CH2_PIN					GPIO_Pin_15

#define	M_P_RCC_APBPeriph_TIMx_GPIOx3		RCC_APB2Periph_GPIOE
#define M_P_RCC_APBxPeriphClockCmd3         RCC_APB2PeriphClockCmd
#define	M_RCC_APBPeriph_TIMx3				RCC_APB2Periph_TIM9
#define M_RCC_APBxPeriphClockCmd3           RCC_APB2PeriphClockCmd
#define	MOTOR3_PORT							GPIOE
#define MOTOR3_TIM							TIM9
#define MOTOR3_TIM_CH1_PIN					GPIO_Pin_5
#define MOTOR3_TIM_CH2_PIN					GPIO_Pin_6

#define motor_1 1
#define motor_2 2
#define motor_3 3
#define motor_4 4
#define motor_5 5
#define motor_6 6
#define motor_7 7
#define motor_8 8

extern void TIM_MOTOR_Init(void);
extern void Motor_Run(int motor_num,int speed_set);
extern void Test_Motor(void);

#endif /* _MORTOR_H_ */
