#include "servo.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "delay.h"

/**
  * @brief	      重映射
  * @param        无
  * @retval       只对TIM13和14进行重映射
  * @author       718 Lab
  */
static void AT32_TIM13_TIM14_Remap(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);//开重映射时钟
  AFIO->MAPR2&=(0xfffffCff);//先对目标位置清空
  AFIO->MAPR2|=0x00000300;//对相应寄存器进行填写
}

/**
  * @brief	      高级定时器初始化
  * @param        无
  * @retval       无
  * @author       718 Lab
  */
void TIM_SERVO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure; //GPIO初始化结构体
    //输出比较通道1、2、3 GPIO初始化
    S_P_RCC_APBxPeriphClockCmd1(S_P_RCC_APBPeriph_TIMx_GPIOx1, ENABLE); //使能外设总线时钟
    S_P_RCC_APBxPeriphClockCmd2(S_P_RCC_APBPeriph_TIMx_GPIOx2, ENABLE);
    S_P_RCC_APBxPeriphClockCmd3(S_P_RCC_APBPeriph_TIMx_GPIOx3, ENABLE);
    S_P_RCC_APBxPeriphClockCmd4(S_P_RCC_APBPeriph_TIMx_GPIOx4, ENABLE);
    /*RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE); //shut JTAG

    GPIO_PinRemapConfig(GPIO_FullRemap_TIM2, ENABLE);*/
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;                                                                  //复用推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;                                                                //设置输出频率
    GPIO_InitStructure.GPIO_Pin = SERVO1_TIM_CH1_PIN ; //设置GPIO管脚
    GPIO_Init(SERVO1_PORT, &GPIO_InitStructure);                                                                     //初始化GPIO

    GPIO_InitStructure.GPIO_Pin = SERVO2_TIM_CH1_PIN; //设置GPIO管脚
    GPIO_Init(SERVO2_PORT, &GPIO_InitStructure);                                                //初始化GPIO

    GPIO_InitStructure.GPIO_Pin = SERVO3_TIM_CH1_PIN; //设置GPIO管脚
    GPIO_Init(SERVO3_PORT, &GPIO_InitStructure);      //初始化GPIO

    GPIO_InitStructure.GPIO_Pin = SERVO4_TIM_CH1_PIN; //设置GPIO管脚
    GPIO_Init(SERVO4_PORT, &GPIO_InitStructure);      //初始化GPIO

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure; //定时器时基结构体
    TIM_OCInitTypeDef TIM_OCInitStructure;         //定时器输出比较结构体
		AT32_TIM13_TIM14_Remap();

    S_RCC_APBxPeriphClockCmd1(S_RCC_APBPeriph_TIMx1, ENABLE); //使能定时器时钟
    S_RCC_APBxPeriphClockCmd2(S_RCC_APBPeriph_TIMx2, ENABLE); //使能定时器时钟
    S_RCC_APBxPeriphClockCmd3(S_RCC_APBPeriph_TIMx3, ENABLE);
    S_RCC_APBxPeriphClockCmd4(S_RCC_APBPeriph_TIMx4, ENABLE);

    /*--------------------时基结构体初始化-------------------------*/
    // 配置周期，这里配置为0.005s
    TIM_TimeBaseStructure.TIM_Period = 5000 - 1;               //自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
    TIM_TimeBaseStructure.TIM_Prescaler = 71;                   //驱动CNT计数器的时钟 = Fck_int/(psc+1)
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;     //时钟分频因子 ，输出互补脉冲配置死区时间需要用到
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //计数器计数模式，设置为向上计数
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;            //设置重复计数器的值为0，不设置重复计数
    TIM_TimeBaseInit(SERVO1_TIM, &TIM_TimeBaseStructure);       //初始化定时器
    TIM_TimeBaseInit(SERVO2_TIM, &TIM_TimeBaseStructure);       //初始化定时器
    TIM_TimeBaseInit(SERVO3_TIM, &TIM_TimeBaseStructure);       //初始化定时器
    TIM_TimeBaseInit(SERVO4_TIM, &TIM_TimeBaseStructure);       //初始化定时器
    /*--------------------输出比较结构体初始化-------------------*/
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;             //选择定时器模式:TIM脉冲宽度调制模式1
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //使能输出比较
    TIM_OCInitStructure.TIM_Pulse = 0;                            //设置初始占空比
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;     //输出极性:TIM输出比较极性高(高电平有效)

    TIM_OC1Init(SERVO1_TIM, &TIM_OCInitStructure);          //初始化输出比较通道1
    TIM_OC1PreloadConfig(SERVO1_TIM, TIM_OCPreload_Enable); //使能通道1的CCR4上的预装载寄存器

    TIM_OC1Init(SERVO2_TIM, &TIM_OCInitStructure);          //初始化输出比较通道1
    TIM_OC1PreloadConfig(SERVO2_TIM, TIM_OCPreload_Enable); //使能通道1的CCR4上的预装载寄存器

    TIM_OC1Init(SERVO3_TIM, &TIM_OCInitStructure);          //初始化输出比较通道1
    TIM_OC1PreloadConfig(SERVO3_TIM, TIM_OCPreload_Enable); //使能通道1的CCR4上的预装载寄存器

    TIM_OC1Init(SERVO4_TIM, &TIM_OCInitStructure);          //初始化输出比较通道1
    TIM_OC1PreloadConfig(SERVO4_TIM, TIM_OCPreload_Enable); //使能通道1的CCR4上的预装载寄存器


    TIM_Cmd(SERVO1_TIM, ENABLE);            //使能TIM
    //TIM_CtrlPWMOutputs(SERVO1_TIM, ENABLE); //主输出使能，当使用的是通用定时器，不需要主输出使能

    TIM_Cmd(SERVO2_TIM, ENABLE);            //使能TIM
    //TIM_CtrlPWMOutputs(SERVO2_TIM, ENABLE); //主输出使能，当使用的是通用定时器，不需要主输出使能

    TIM_Cmd(SERVO3_TIM, ENABLE);            //使能TIM
    //TIM_CtrlPWMOutputs(SERVO3_TIM, ENABLE); //主输出使能，当使用的是通用定时器，不需要主输出使能

    TIM_Cmd(SERVO4_TIM, ENABLE);            //使能TIM
    //TIM_CtrlPWMOutputs(SERVO3_TIM, ENABLE); //主输出使能，当使用的是通用定时器，不需要主输出使能
}

/**
  * @brief	      舵机控制函数
  * @param        无
  * @retval       无
  * @note         使用时注意一下舵机中值的占空比、PWM频率、舵机打角范围对应的占空比
  * @author       718 Lab
  */
void Servo_Open(void)
{
    Servo_Run(servo_1, 2000);
    Servo_Run(servo_2, 2000);
    Servo_Run(servo_3, 2000);
    Servo_Run(servo_4, 2000);
}

/**
  * @brief	      舵机控制函数
  * @param        无
  * @retval       无
  * @note         使用时注意一下舵机中值的占空比、PWM频率、舵机打角范围对应的占空比
  * @author       718 Lab
  */
void Servo_Close(void)
{
    Servo_Run(servo_1, 600);
    Servo_Run(servo_2, 600);
    Servo_Run(servo_3, 600);
    Servo_Run(servo_4, 600);
}

/**
  * @brief	      舵机测试函数
  * @param        无
  * @retval       无
  * @note         使用时注意一下舵机中值的占空比、PWM频率、舵机打角范围对应的占空比
  * @author       718 Lab
  */
void Test_Servo(void)
{
    Servo_Run(servo_1, 1500);
    Servo_Run(servo_2, 1500);
    Servo_Run(servo_3, 1500);
    Servo_Run(servo_4, 1500);
}

/**
  * @brief	      电机控制函数
  * @param	      servo_num 可以填入motor_1/motor_2/motor_3/motor_4/motor_5/motor_6/motor_7/motor_8来选择控制电机1/2/3/4
  * @param	      speed_set PWM高电平的时间。可以填入速度设定值来控制电机转速.范围500-2500
  * @retval       无
  * @author       718 Lab
  */

void Servo_Run(int servo_num, int speed_set)
{
    switch (servo_num)
    {
    case servo_1:
        TIM_SetCompare1(SERVO1_TIM, speed_set);
        break;
    case servo_2:
        TIM_SetCompare1(SERVO2_TIM, speed_set);
        break;
    case servo_3:
        TIM_SetCompare1(SERVO3_TIM, speed_set);
        break;
    case servo_4:
        TIM_SetCompare1(SERVO4_TIM, speed_set);
        break;
    default:
        break;
    }
}

