#include "bee.h"

/**
  * @brief        与 BEE 对应的 GPIO 端口初始化函数
  * @param        无
  * @retval       无
  * @author       718 Lab
***/
void BEE_GPIO_Init(void)
{
    /* 定义一个 GPIO_InitTypeDef 类型的结构体 */
    GPIO_InitTypeDef GPIO_InitStructure;

    /* 开启 LED 相关的 GPIO 外设时钟 */
    RCC_APB2PeriphClockCmd(BEE_GPIO_CLK, ENABLE);
    /* 选择要控制的 GPIO 引脚 */
    GPIO_InitStructure.GPIO_Pin = BEE_GPIO_PIN;
    /* 设置引脚模式为通用推挽输出 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    /* 设置引脚速率为 50MHz */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    /* 调用库函数，初始化 GPIO */
    GPIO_Init(BEE_GPIO_PORT, &GPIO_InitStructure);
}

/**
  * @brief        控制 BEE 对应的 GPIO 引脚的状态
  * @param        State 需要设置的状态(0 为低电平 1 为高电平)
  * @retval       无
  * @author       718 Lab
***/
void BEE_Set(uint8_t State)
{
    if (State == 1)
				GPIO_ResetBits(BEE_GPIO_PORT, BEE_GPIO_PIN);
		else if (State == 0)
				GPIO_SetBits(BEE_GPIO_PORT, BEE_GPIO_PIN);
}
